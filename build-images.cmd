docker build --rm -f ./Docker/node.Dockerfile -t rammbulanz/node .
docker build --rm -f ./Docker/nodemon.Dockerfile -t rammbulanz/nodemon .
docker build --rm -f ./Docker/tsc.Dockerfile -t rammbulanz/tsc .
docker build --rm -f ./Docker/webpack.Dockerfile -t rammbulanz/webpack .
docker build --rm -f ./Docker/polymer-cli.Dockerfile -t rammbulanz/polymer-cli .
docker build --rm -f ./Docker/yarn.Dockerfile -t rammbulanz/yarn .
docker build --rm -f ./Docker/gulp.Dockerfile -t rammbulanz/gulp .