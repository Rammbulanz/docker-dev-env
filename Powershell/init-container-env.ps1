# Currently commands end with "-docker"
# Will change later in names without docker
# Example: "node-docker" > "node"

Write-Host "Initializing Dockerized Development environment"

$DOCKER_CALL_PREFIX = 'docker run --rm -it -v '
$DOCKER_CALL_SUFFIX = ':/workload -w /workload '

$NODE_IMAGE = 'rammbulanz/node'
$NODEMON_IMAGE = 'rammbulanz/nodemon'
$POLYMER_IMAGE = 'rammbulanz/polymer-cli'
$TSC_IMAGE = 'rammbulanz/tsc'
$YARN_IMAGE = 'rammbulanz/node'
$WEBPACK_IMAGE = 'rammbulanz/node'
$GULP_IMAGE = 'rammbulanz/gulp'

function CheckForDocker {
	Write-Host "Checking for Docker..."
	where.exe docker.exe
	if (-not ($LASTEXITCODE -eq 0)) {
		Write-Host "Can't find docker.exe. Please install and try again."
		exit 1
	}
	else {
		Write-Host "Found Docker"
	}
}
function CheckDockerImages {
	Write-Host "Checking for Dockerimages..."
	CheckDockerImage -Image $NODE_IMAGE
	CheckDockerImage -Image $NODEMON_IMAGE
	CheckDockerImage -Image $POLYMER_IMAGE
	CheckDockerImage -Image $TSC_IMAGE
	CheckDockerImage -Image $YARN_IMAGE
	CheckDockerImage -Image $WEBPACK_IMAGE
	CheckDockerImage -Image $GULP_IMAGE
}

function CheckDockerImage {
	Param($Image)
	$imageID = `docker.exe images -q $Image | findstr.exe .`
	if ($LASTEXITCODE -eq 1) {
		Write-Host $Image " not found, pulling it"
		docker.exe pull $Image
	}
 	else {
		Write-Host 'Found: ' $imageID ' => ' $Image
	}
}

function CheckDockerVolume {
	Param($Volume)
	`docker.exe volume ls --quiet --filter name=$Volume | findstr.exe .`
	if ($LASTEXITCODE -eq 1) {
		Write-Host $Volume " not found, creating it"
		docker.exe volume create $Volume
	}
	else {
		Write-Host 'Volume ' + $Volume + ' found'
	}
}

CheckForDocker
CheckDockerImages

Write-Host "Initializing docker functions"

function Get-DockerCall {
	Param($Image, $DockerOpts, $CustomDockerOpts)
	$command = $DOCKER_CALL_PREFIX + ${pwd} + $DOCKER_CALL_SUFFIX

	if ($CustomDockerOpts) {
		$command += $CustomDockerOpts + " "
	}
	 
	if (Test-Path Env:$DockerOpts) {
		$opts = Get-Content Env:$DockerOpts
		$command += $opts + " "
	}

	$command += $Image
	return $command
}

function node-docker {
	$command = Get-DockerCall -Image $NODE_IMAGE -DockerOpts 'NODE_DOCKER_OPTS'
	$command += " " + $Args
	Invoke-Expression $command
}

function npm-docker {

	$customOpts = $null
	if (Test-Path $env:USERPROFILE\.yarnrc) {
		$customOpts = "-v " + $env:USERPROFILE + "\.yarnrc:/root/.yarnrc "
	}

	if (Test-Path $env:USERPROFILE\.npmrc) {
		if ($customOpts) {
			$customOpts += "-v " + $env:USERPROFILE + "\.npmrc:/root/.npmrc "
		}
		else {
			$customOpts = "-v " + $env:USERPROFILE + "\.npmrc:/root/.npmrc "
		}
	}

	$customOpts += "-v " + $env:LOCALAPPDATA + "\yarn__docker_cache:/usr/local/share/.cache/yarn "

	$command = Get-DockerCall -Image $YARN_IMAGE -DockerOpts 'YARN_DOCKER_OPTS' -CustomDockerOpts $customOpts
	$command += " npm " + $Args

	Invoke-Expression $command
}

function nodemon-docker {
	$command = Get-DockerCall -Image $NODEMON_IMAGE -DockerOpts 'NODEMON_DOCKER_OPTS'
	$command += " " + $Args
	Invoke-Expression $command
}

function tsc-docker {
	$command = Get-DockerCall -Image $TSC_IMAGE -DockerOpts 'TSC_DOCKER_OPTS'
	$command += " " + $Args
	Invoke-Expression $command
}

function yarn-docker {
	$customOpts = $null
	if (Test-Path $env:USERPROFILE\.yarnrc) {
		$customOpts = "-v " + $env:USERPROFILE + "\.yarnrc:/root/.yarnrc "
	}

	if (Test-Path $env:USERPROFILE\.npmrc) {
		if ($customOpts) {
			$customOpts += "-v " + $env:USERPROFILE + "\.npmrc:/root/.npmrc "
		}
		else {
			$customOpts = "-v " + $env:USERPROFILE + "\.npmrc:/root/.npmrc "
		}
	}

	if (-Not (Test-Path -Path $env:LOCALAPPDATA\yarn__docker_cache)) {
		New-Item $env:LOCALAPPDATA\yarn__docker_cache -ItemType Directory
	}

	$customOpts += "-v " + $env:LOCALAPPDATA + "\yarn__docker_cache:/usr/local/share/.cache/yarn "

	$command = Get-DockerCall -Image $YARN_IMAGE -DockerOpts 'YARN_DOCKER_OPTS' -CustomDockerOpts $customOpts
	$command += " yarn " + $Args

	Invoke-Expression $command
}

function polymer-docker {
	$command = Get-DockerCall -Image $POLYMER_IMAGE -DockerOpts ' -p 8081:8081'
	$command += " " + $Args
	Invoke-Expression $command
}

function gulp-docker {
	$command = Get-DockerCall -Image $GULP_IMAGE -DockerOpts 'GULP_DOCKER_OPTS'
	$command += " " + $Args
	Invoke-Expression $command
}

function portainer {
	CheckDockerVolume -Volume portainer_data
	$command = 'docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer'
	Invoke-Expression $command
}

function formatHelpCommand {
	Param($Command, $Help, $Opts)
	Write-Host -ForegroundColor DarkYellow -NoNewline $Command
	Write-Host " => " $Help -NoNewline
	Write-Host " (use " -NoNewline
	Write-Host -ForegroundColor White -NoNewline $Opts
	Write-Host " in order to pass options to the docker call)"
}

function Get-DevContainerHelp {
	Write-Host "the following functions are provided"
	formatHelpCommand -Command "node-docker" -Help "runs node v10" -Opts "NODE_DOCKER_OPTS"
	formatHelpCommand -Command "nodemon-docker" -Help "runs nodemon" -Opts "NODEMON_DOCKER_OPTS"
	formatHelpCommand -Command "tsc-docker" -Help "runs tsc 3.0.3" -Opts "TSC_DOCKER_OPTS"
	formatHelpCommand -Command "yarn-docker" -Help "runs yarn" -Opts "YARN_DOCKER_OPTS"
	formatHelpCommand -Command "polymer-docker" -Help "runs polymer" -Opts "POLYMER_DOCKER_OPTS"
	formatHelpCommand -Command "webpack-docker" -Help "runs webpack" -Opts "WEBPACK_DOCKER_OPTS"
}

Write-Host "All done type " -NoNewline
Write-Host -ForegroundColor DarkYellow -NoNewline "Get-DevContainerHelp"
Write-Host " for more information"
Write-Host "Happy dockering..."

