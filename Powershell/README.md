# Docker Development container's

provided are a set of docker images to be used with projects using typescript, nodemon for debugging during development.

- rammbulanz/nodemon
- rammbulanz/tsc
- rammbulanz/polymer-cli
- rammbulanz/webpack

`build-images.cmd` builds all images at once
### With powershell

if not existing create a profile script that is run every time you fire up powershell
1. open a powershell
2. `Set-ExecutionPolicy -Scope CurrentUser RemoteSigned`
3. `$profile.CurrentUserCurrentHost` should display the initial script to be loaded for the current user on local machine
4. `Test-Path $profile.CurrentUserCurrentHost` returns True if the file already exists, false if not
5. if file is not existing `New-Item $profile.CurrentUserCurrentHost` will create it and display informtion about it
6. open the file in your favorite editor
7. append the following lines
```powershell
$profilePath = Split-Path -Path $profile.CurrentUserCurrentHost
$devContainerEnv = Join-Path -Path $profilePath -ChildPath "init-container-env.ps1"
Get-ChildItem  $devContainerEnv  | %{.$_}
```
8. copy `Powershell/\init-dev-container-env.ps1` to the profile path (from step 3)

now everytime you open a powershell the `init-dev-container-env.ps1` script should be run providing functions on the command line with work with the development containers.

### From docker-compose
- either reference the images directly if you built them yourself

example docker-compose.yml

```
version "3"
services:
    tsc-watcher:
        image: rammbulanz/tsc
        user: "node"
        volumes:
            - ./:/home/node/app
        working_dir: /home/node/app
        command: -w
```

above snippet will run the tsc-docker image watching /home/node/app inside container from
****command: -w*** and recompile if source files are changing.

### From VS Code
command lines from above can easy be incorporated into the projects package.json scripts.
without additional packages however those scripts won't be cross-platform compatible due to differences in how environment variables are accessed.

example script to install yarn dependencies

```json
{
    "install-docker": "docker run --rm -v %CD%:/home/node/app -w /home/node/app yarn-docker install"
}
```

