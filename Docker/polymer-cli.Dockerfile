FROM node:10
RUN apt-get install -y --no-install-recommends git
RUN npm install -g polymer-cli --unsafe-perm
EXPOSE 8081
ENTRYPOINT [ "polymer" ]