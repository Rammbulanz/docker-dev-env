FROM node:10-alpine
RUN npm i -g webpack-cli webpack
ENTRYPOINT [ "webpack" ]