FROM node:10-alpine
RUN npm i -g typescript
ENTRYPOINT [ "tsc" ]